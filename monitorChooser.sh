#!/bin/bash
choice1=$(xrandr -q | grep -w connected | awk '{print $1}' |  sed 's/ \+/\n/g' | dmenu -p "Choose ur display")
choice2=$(echo -e "set right of\nset left of\nresolution\nset primary\non/off" | dmenu)
case "$choice2" in
"set right of") monitorLeft=$(xrandr -q | grep -w connected | awk '{print $1}' |  sed 's/ \+/\n/g' | dmenu -p "Right of what?")
xrandr --output $choice1 --right-of $monitorLeft ;;
"set left of")  monitorRight=$(xrandr -q | grep -w connected | awk '{print $1}' |  sed 's/ \+/\n/g' | dmenu -p "left of what?")
xrandr --output $choice1 --left-of $monitorRight ;;
"resolution") res=$(xrandr |
  awk -v monitor="^$choice1 connected" '/connected/ {p = 0}
    $0 ~ monitor {p = 1}
    p' | grep -v "$choice1" | awk '{print $1}' | dmenu)
xrandr --output $choice1 --mode $res ;;
"set primary") xrandr --output $choice1 --primary ;;
"on/off") maxRes=$(xrandr |
  awk -v monitor="^$choice1 connected" '/connected/ {p = 0}
    $0 ~ monitor {p = 1}
    p' | grep -v "$choice1" | awk '{print $1}' | head -n 1)
xrandr |
  awk -v monitor="^$choice1 connected" '/connected/ {p = 0}
    $0 ~ monitor {p = 1}
    p' | grep -v "$choice1" | grep "*"
if [ $? -eq 0 ]
then
xrandr --output $choice1 --off
else
xrandr --output $choice1 --auto
fi
;;
esac
